import Home from "../views/Home.vue";
import Utilisateurs from "../views/Users.vue";
import Utilisateur from "../views/User.vue";
import Albums from "../views/Albums.vue";
import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/utilisateurs",
    name: "Utilisateurs",
    component: Utilisateurs
  },
  {
    path: "/utilisateur/:id",
    name: "Utilisateur",
    component: Utilisateur
  },
  {
    path: "/albums",
    name: "Albums",
    component: Albums
  }
];

const router = new VueRouter({
  routes
});

export default router;
