const axios = require("axios");

let todoService = {
  getTodos() {
    return axios.get("https://jsonplaceholder.typicode.com/todos");
  },
  getTodosByUserId(id) {
    return axios.get(
      "https://jsonplaceholder.typicode.com/users/" + id + "/todos"
    );
  }
};

export default todoService;
