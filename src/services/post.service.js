const axios = require("axios");

let postService = {
  getPosts() {
    return axios.get("https://jsonplaceholder.typicode.com/posts");
  },
  getPostsByUserId(id) {
    return axios.get(
      "https://jsonplaceholder.typicode.com/users/" + id + "/posts"
    );
  }
};

export default postService;
