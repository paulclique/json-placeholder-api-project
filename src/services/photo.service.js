const axios = require("axios");

let photoService = {
  getPhotos() {
    return axios.get("https://jsonplaceholder.typicode.com/photos");
  },
  getPhotosByAlbumId(id) {
    return axios.get(
      "https://jsonplaceholder.typicode.com/album/" + id + "/photos"
    );
  }
};

export default photoService;
