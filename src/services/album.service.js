const axios = require("axios");

let albumService = {
  getAlbums() {
    return axios.get("https://jsonplaceholder.typicode.com/albums");
  },
  getAlbumsByUserId(id) {
    return axios.get(
      "https://jsonplaceholder.typicode.com/users/" + id + "/albums"
    );
  }
};

export default albumService;
