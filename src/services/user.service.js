const axios = require("axios");

let userService = {
  getUsers() {
    return axios.get("https://jsonplaceholder.typicode.com/users");
  },
  getUserById(id) {
    return axios.get("https://jsonplaceholder.typicode.com/users/" + id);
  }
};

export default userService;
