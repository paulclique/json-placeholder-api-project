const axios = require("axios");

let albumService = {
  getCommentsByPostID(id) {
    return axios.get(
      "https://jsonplaceholder.typicode.com/posts/" + id + "/comments"
    );
  }
};

export default albumService;
